name := "scalajs-webpack-react"

scalaVersion := "2.13.4"

enablePlugins(ScalaJSBundlerPlugin)

scalaJSUseMainModuleInitializer := true

webpackBundlingMode := BundlingMode.LibraryAndApplication()

// Automatically reload the build when source changes are detected
Global / onChangedBuildSource := ReloadOnSourceChanges

npmDependencies in Compile ++= Seq(
    "react" -> "17.0.1",
    "react-dom" -> "17.0.1"
)

npmDevDependencies in Compile ++= Seq(
    "webpack-merge" -> "4.2.2",
    "@babel/core" -> "7.12.10",
    "@babel/preset-env" -> "7.12.11",
    "@babel/preset-react" -> "7.12.10",
    "babel-loader" -> "8.2.2"
)

val webpackDir = Def.setting {
  (baseDirectory in ThisProject).value / "webpack"
}

val webpackDevConf = Def.setting {
    Some(webpackDir.value / "webpack.config.js")
}

webpackResources := webpackDir.value * "*"
webpackConfigFile in fastOptJS := webpackDevConf.value

// Start webpack dev server on SBT startup
onLoad in Global := (onLoad in Global).value.andThen(state => "fastOptJS::startWebpackDevServer" :: state)