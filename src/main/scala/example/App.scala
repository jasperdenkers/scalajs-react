package example

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExportTopLevel, JSExport, JSImport}

object App {
  def main(args: Array[String]): Unit = {
    println("Hello world! (Scala.js)")
    JsApp.helloWorld()
    println("  foo: " + JsApp.foo)
    println("  bar(1): " + JsApp.bar(1))
  }
}

@JSImport("../../../../src/main/js/app.js", JSImport.Namespace)
@js.native
object JsApp extends js.Object {
  def helloWorld(): Unit = js.native
  def foo: Int = js.native
  def bar(i: Int): Int = js.native
}

@JSExportTopLevel("common")
object Common {
  @JSExport
  val baz = 42
}
