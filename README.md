# Scala.js + React

An example project that integrates [Scala.js](https://www.scala-js.org/) and [React](https://reactjs.org/).

Powered by [scalajs-bundler](https://scalacenter.github.io/scalajs-bundler/), which bundles Scala.js with npm packages using [npm](https://www.npmjs.com/) and [webpack](https://webpack.js.org/).

## Prerequisites 
 
 - The [SBT](https://www.scala-sbt.org/download.html) build tool

## Getting Started

Run SBT with `sbt`.
The project is configured to automatically run `fastOptJS::startWebpackDevServer` on startup, which [spawns a background process with webpack's development server](https://scalacenter.github.io/scalajs-bundler/cookbook.html#webpack-dev-server).

Trigger continuous compilation of Scala.js by running (within the SBT shell):

```
~fastOptsJS::webpack
```

Open `index.html` with a browser.

`Hello world!` is reported from several sources, demonstrating the integration of Javascript generated from Scala.js, regular Javascipt, Scala.js and Javascript calling each other, and React.

## Development

While `~fastOptsJS::webpack` is running, changes to Scala sources will automatically trigger recompilation.
While the webpack development server is running, changes to both Scala as Javascript sources will trigger live reloading.
